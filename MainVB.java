public class MainVB {
	public static void main() throws InterruptedException {
		//Vengono istanziati 12 clienti ed aggiunti ad un ArrayList di supermercato, succesivamente viene invocato il metodo start per ogni cliente 
		SupermercatoVB s = new SupermercatoVB();
		ClienteVB c1 = new ClienteVB("Alberto", "Via Roma 24", s);
		ClienteVB c2 = new ClienteVB("Federico", "Via Milano 42", s);
		ClienteVB c3 = new ClienteVB("Valeria", "Via Torino 3", s);
		ClienteVB c4 = new ClienteVB("Salvatore", "Via Respighi 12", s);
		ClienteVB c5 = new ClienteVB("Antonio", "Via Leopardi 6", s);
		ClienteVB c6 = new ClienteVB("Attilio", "Via Leonardo Da Vinci 8", s);
		ClienteVB c7 = new ClienteVB("Umberto", "Via Caravaggio 9", s);
		ClienteVB c8 = new ClienteVB("Giovanni", "Via Catalani 74", s);
		ClienteVB c9 = new ClienteVB("Sara", "Via Boito 2", s);
		ClienteVB c10 = new ClienteVB("Anna", "Via De Luca 3", s);
		ClienteVB c11 = new ClienteVB("Francesca", "Via Meucci 5", s);
		ClienteVB c12 = new ClienteVB("Martina", "Via Mattei 14", s);
				
		s.getClienti().add(c1);
		s.getClienti().add(c2);
		s.getClienti().add(c3);
		s.getClienti().add(c4);
		s.getClienti().add(c5);
		s.getClienti().add(c6);
		s.getClienti().add(c7);
		s.getClienti().add(c8);
		s.getClienti().add(c9);
		s.getClienti().add(c10);
		s.getClienti().add(c11);
		s.getClienti().add(c12);
		
		c1.start();
		c2.start();
		c3.start();
		c4.start();
		c5.start();
		c6.start();
		c7.start();
		c8.start();
		c9.start();
		c10.start();
		c11.start();
		c12.start();
		
		//Attesa che tutti i clienti terminino
		while(c1.isAlive() || c2.isAlive() || c3.isAlive() || c4.isAlive() || c5.isAlive() || c6.isAlive() || c7.isAlive() || c8.isAlive() || c9.isAlive() || c10.isAlive() || c11.isAlive() || c12.isAlive()); 
		
		s.getPoolFurgoncini().shutdownNow(); //Termina i furgoncini che sono sempre in esecuzione, viene fatta una sola execute nel costruttore di Supermercato
		s.getPoolPersonale().shutdown(); //Il personale non � sempre in esecuzione, in quanto viene fatta la execute del thread pool nel metodo preparaOrdine() di Supermercato
	}
}
