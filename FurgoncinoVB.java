
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FurgoncinoVB extends Thread {
	private String targa;
	private SupermercatoVB s;
	private boolean carico;
	private List<Pacchetto> Bagagliaio;
	
	public FurgoncinoVB(String targa, SupermercatoVB s) {
		this.targa = targa;
		this.s = s;
		this.carico = false;
		this.Bagagliaio = new ArrayList<Pacchetto>();
	}
	
	public void run() {
		while(true) {
			try {
				s.caricaPacchetto(this); //carica l'pacchetto dal magazzino del supermercato al bagagliaio
				this.consegnaSpesa(); 
				s.notifica();
			} catch (InterruptedException e) { //viene catturata l'eccezione quando nel main viene eseguita la shutdownNow()
				System.exit(0);
			}
		}
	}
	
	public void consegnaSpesa() throws InterruptedException {
		String s = "Il furgoncino (targa: " + this.targa + ") e' partito dal supermercato con " + this.Bagagliaio.size() + " ordine/i: ";
		
		for (int i = 0; i < Bagagliaio.size(); i++) {
			s += this.Bagagliaio.get(i).toString() + " ";
		}
		System.out.println(s); //stampa messaggio di partenza
		
		for (int i = 0; i < Bagagliaio.size(); i++) {
			Thread.sleep((int)(Math.random() * 10000)); //consegna la spesa a domicilio
			this.cercaCliente(this.Bagagliaio.get(i).getIdOrdine());
			System.out.println("Il furgoncino (targa: " + this.targa + ") ha consegnato la spesa (ordine: " + this.Bagagliaio.get(i).getIdOrdine() + ") all'indirizzo: " + this.Bagagliaio.get(i).getIndirizzo() + ".");
		}
		this.Bagagliaio.clear();
		this.setCarico(false);
	}
	
	public void caricaPacchetto (Pacchetto p) { //viene aggiunto il pacchetto p al bagagliaio (la dimensione massima del bagagliaio � di 2 pacchetti)
		this.Bagagliaio.add(p);
		if(Bagagliaio.size() == 2)
			this.setCarico(true);
	}
	
	public void cercaCliente(UUID idOrdine) { //cerca il cliente nella lista clienti del supermercato per segnalare l'avvenuta consegna
		boolean trovato = false;
		
		for (int i = 0; i < s.getClienti().size() && !trovato; i++) {
			if(s.getClienti().get(i).getIdOrdine().compareTo(idOrdine) == 0) {
				s.getClienti().get(i).setConsegnato(true); //avvenuta consegna
				trovato = true;
			}
		}
	}

	//setters
	public void setCarico(boolean carico) {
		this.carico = carico;
	}
	
	public boolean isCarico() {
		return carico;
	}
}