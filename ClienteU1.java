import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class ClienteU1 {
	
	private static Scanner tastiera = new Scanner(System.in);
	
	public static void startClient() {
		Socket socket;
		InetAddress serverAddress;
		int serverPort;
		String serverResponse = "";
		String command = "";
		String nomeUtente = "";	
		boolean domiciliato = false;
		PrintWriter out;
		char risp;
		
		System.out.println("--------------------Cliente--------------------");
		
		try {
			serverAddress = InetAddress.getByName("localhost");
			serverPort = 53000;
			socket = new Socket(serverAddress, serverPort);
			System.out.println("Connected to: " + socket.getInetAddress());
			BufferedInputStream in = new BufferedInputStream (socket.getInputStream());
			out = new PrintWriter(socket.getOutputStream());
			
			
			System.out.println("Benvenuto nel sistema ConsegnaSpesa24!");
			
			do {
				System.out.print("Inserire il nomeUtente (non deve contenere spazi): ");
				nomeUtente = tastiera.nextLine();
			}while(controllaNomeUtente(nomeUtente)); //se il nome utente contiene spazi viene richiesto di reinserirlo
			
			do {
				command = menu(nomeUtente); //il metodo menu() stampa a schermo il men� e memorizza le scelte del cliente in una stringa (commmand) che viene ritornata
				
				domiciliato = estraiScelta(command); //viene estratta la scelta del domiciliato che viene salvata in un booleano 
				
				out.print(command); // viene mandato un messaggio contenente le scelte del cliente al server
				out.flush();
				System.out.println("Ordine inviato al server");

				serverResponse = readFromServer(in); //viene ricevuta la presa in carico
				System.out.println(serverResponse);
				
				if(serverResponse.equals("Prenotazione fallita! Tutto il personale e' impegnato a soddisfare altre richieste, riprovare piu' tardi.")) { // se la prenotazione
																																						 //fallisce viene chiesto
																																						 //se si vuole effettuare 
																																						 //un nuovo ordine
				} else if (domiciliato) {
					serverResponse = readFromServer(in); //viene ricevuto un messaggio che informa il cliente dell'avvenuta partenza del pacchetto dal supermercato
					System.out.println(serverResponse);
					
					serverResponse = readFromServer(in); //viene ricevuto un messaggio che informa il cliente dell'avvenuta consegna
					System.out.println(serverResponse);
				}else {
					serverResponse = readFromServer(in); //viene ricevuto un messaggio che informa il cliente che il pacchetto � pronto per il ritiro
					
					do {
						System.out.println(serverResponse);
						System.out.println("Vuoi ritirare l'ordine? [y/n]");
						risp = tastiera.nextLine().charAt(0);
					}while(risp != 'y' && risp != 'Y'); //finch� il cliente non risponde con 'Y' oppure 'y' viene stampata di nuovo la domanda
					
					out.print("ok"); //se il cliente risponde di si viene mandato un messaggio al server per avvisarlo che il cliente ritira il pacchetto
					out.flush();
					
					serverResponse = readFromServer(in); //riceve l'idOrdine del pacchetto che ha ritirato
					
					System.out.println("Pacchetto " + serverResponse + " ritirato");
				}
				
				System.out.println("Vuoi effettuare un nuovo ordine? [y/n]");
				risp = tastiera.nextLine().charAt(0);
			}while(risp == 'y' || risp == 'Y'); //se il cliente vuole effettuare un nuovo ordine viene rieseguito il codice dal men� in poi
			
			out.print("stop"); //se il cliente decide di non voler effettuare un nuovo ordine viene mandato al server un messaggio per avvisarlo che vuole chiudere la connessione
			out.flush();
			socket.close();
			System.out.println("Connessione chiusa");
			System.out.println("Arrivederci e grazie per aver usufruito del servizio ConsegnaSpesa24!");
			
		}catch(IOException e) {
			e.printStackTrace();
		}		
	}

	public static String menu(String nomeUtente) { //stampa a video il menu con le scelte e i dati da inserire e memorizza le risposte in command
		String command = "";		
	
		System.out.println("Specificare se si preferisce:");
		do{
			command = "";
			System.out.println("[1] Consegna a domicilio");
			System.out.println("[2] Ritiro presso il supermercato");
			command = tastiera.nextLine() + ";";
		}while(!command.equals("1;")&&!command.equals("2;")); //se viene scritto qualcosa di diverso da 1 oppure 2 vengono stampate di nuovo le due scelte finch� il cliente
															  //non scrive uno dei due numeri accettati
		
		if(command.equals("1;")){ //solo se il cliente ha scelto la consegna a domicilio viene richiesto l'indirizzo senn� viene concatenata la stringa "null" 
								  //al posto dell'indirizzo
			System.out.println("Inserire l'indirizzo: ");
			command += tastiera.nextLine();
		}else{
			command += "null";
		}
				
		command = nomeUtente + ";" + command;
		
		return command;
	}
	
	public static boolean controllaNomeUtente(String nomeUtente) { //viene controllata la sintassi del nome utente (non deve contenere spazi) in caso contrario
																   //viene stampato a video un messaggio di errore
		boolean err = false;
		for(int i = 0; i < nomeUtente.length() && !err; i++) {
			if (nomeUtente.charAt(i) == ' ') {
				System.out.println("Sintassi nome utente errata! Inserire un altro nomeUtente.");
				err = true;
			}
		}
		return err;
	}
	
	public static String readFromServer(BufferedInputStream in) throws IOException { //vengono letti i bytes ricevuti dal server
		String s = "";
		int x = 0;
		byte[] buffer =  new byte[128];
		x = in.read(buffer);
		
		buffer[x] = (byte) '\n';
		
		for (int i = 0; buffer[i] != (byte) '\n'; i++) {
			s += (char) buffer[i];
		}
		
		return s;
	}
	
	public static boolean estraiScelta(String cmd) { //viene ricavata la scelta del domiciliato dalla stringa passata
		boolean fine = false;
		char sceltaDomicilio = ' ';
		for (int i = 0; i < cmd.length() && !fine; i++) {
			if (cmd.charAt(i) == ';') {
				sceltaDomicilio = cmd.charAt(i+1);
				fine = true;
			}
		}
		
		if (sceltaDomicilio == '1')
			return true;
		else
			return false;
	}
}