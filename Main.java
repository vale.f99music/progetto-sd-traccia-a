import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		Scanner t = new Scanner(System.in);
		int risp;
		do {
			menu();
			risp = t.nextInt();
		}while(risp < -1 || risp > 4);
		switch(risp) {
			case 0:
				MainVB.main();
				break;
			case 1:
				ServerU1.startServer();
				break;
			case 2:
				ClienteU1.startClient();
				break;
			case 3:
				ServerU2.startServer();
				break;
			case 4:
				ClienteU2.startClient();
		}
		t.close();
	}
	
	public static void menu() {
		System.out.println("----------Progetto Sistemi Distribuiti - Traccia A----------");
		System.out.println("-------------------Progetto realizzato da:------------------");
		System.out.println("--------------Federico De Iaco & Valeria Froio--------------");
		System.out.println("[0] Versione base");
		System.out.println("[1] Upgrade 1 - Server");
		System.out.println("[2] Upgrade 1 - Cliente");
		System.out.println("[3] Upgrade 2 - Server");
		System.out.println("[4] Upgrade 2 - Cliente");
		System.out.println("[-1] Exit");
		System.out.println("------------------------------------------------------------");
	}

}
