import java.util.UUID;

public class PersonaleU2 extends Thread {
	private String nome;
	private String cognome;
	private String matricola;
	private boolean disponibile;
	private SupermercatoU2 s;
	private UUID idOrdine;
	private String indirizzo;
	private boolean domiciliato;
	
	public PersonaleU2 (String nome, String cognome, String matricola, SupermercatoU2 s) {
		this.nome = nome;
		this.cognome = cognome;
		this.matricola = matricola;
		this.disponibile = true;
		this.s = s;
		this.idOrdine = null;
		this.indirizzo = null;
		this.domiciliato = false;
	}
	
	public void run() {
		Pacchetto p = null;
		System.out.println(this.nome + " " + this.cognome + " [" + this.matricola + "]" + " sta impacchettando l'ordine n." + this.getIdOrdine() + "...");
		try {
			Thread.sleep((int)(Math.random() * 30000)); //impacchetta l'ordine
			p = new Pacchetto(idOrdine, domiciliato, indirizzo); //viene creato il pacchetto contenente la spesa
			System.out.println(this.nome + " " + this.cognome + " [" + this.matricola + "]" + " ha finito di impacchettare l'ordine n." + this.getIdOrdine());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		s.immagazzina(p); //il pacchetto viene messo nel magazzino del supermercato (magazzinoRitiro o magazzinoSpedizioni) a seconda della scelta (domiciliato o meno) del cliente
		this.disponibile = true; //viene reso di nuovo disponibile il personale per impacchettare altri ordini
		s.notifica(); //fa la notifyAll() in preparaOrdine() di Supermercato per segnalare che un personale si � liberato
	}
	
	//metodi getters e setters
	public UUID getIdOrdine() {
		return idOrdine;
	}

	public void setIdOrdine(UUID idOrdine) {
		this.idOrdine = idOrdine;
	}
	
	public String getMatricola() {
		return matricola;
	}

	public boolean disponibile() {
		return this.disponibile;
	}

	public void setDisponibile(boolean disponibile) {
		this.disponibile = disponibile;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public void setDomiciliato(boolean domiciliato) {
		this.domiciliato = domiciliato;
	}
}
