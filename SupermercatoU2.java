import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SupermercatoU2 {
	private ExecutorService poolPersonale;
	private ExecutorService poolFurgoncini;
	private PersonaleU2[] personale;
	private FurgoncinoU2[] furgoncini;
	private List<Pacchetto> magazzinoSpedizioni;
	private List<Pacchetto> magazzinoRitiro;
	private List<String> ordini;
	private List<Boolean> ordiniConsegnati;
	private int visible;
	private String[][] tabellaSlot;
	
	public SupermercatoU2() {
		this.poolPersonale = Executors.newFixedThreadPool(20); //nuovo pool di thread fissato a 20 in quanto al massimo vogliamo che 20 threads di personale vengano eseguiti
        this.personale = new PersonaleU2 [20]; //vengono inseriti due oggetti Personale per ogni slot (slot totali 10, quindi servono
        									   //20 persone del personale)
		this.personale[0] = new PersonaleU2("Mario", "Rossi", "001", this); //assegnato a slot 0
		this.personale[1] = new PersonaleU2("Luigi", "Verdi", "002", this); //assegnato a slot 0
		this.personale[2] = new PersonaleU2("Bruno", "Bianchi", "003", this); //assegnato a slot 1
		this.personale[3] = new PersonaleU2("Manlio", "Apriceno", "004", this); //assegnato a slot 1
		this.personale[4] = new PersonaleU2("Adriano", "Firmin", "005", this); //assegnato a slot 2
		this.personale[5] = new PersonaleU2("Patrizia", "Trkuja", "006", this); //assegnato a slot 2
		this.personale[6] = new PersonaleU2("Saveria", "Maioni", "007", this); //assegnato a slot 3
		this.personale[7] = new PersonaleU2("Cornelia", "Baganzani", "008", this); //assegnato a slot 3
		this.personale[8] = new PersonaleU2("Rossella", "Georgieva", "009", this); //assegnato a slot 4
		this.personale[9] = new PersonaleU2("Carlo", "Palermi", "010", this); //assegnato a slot 4
		this.personale[10] = new PersonaleU2("Venere", "Olini", "011", this); //assegnato a slot 5
		this.personale[11] = new PersonaleU2("Giuditta", "Cavelazzi", "012", this); //assegnato a slot 5
		this.personale[12] = new PersonaleU2("Vincenzino", "Speranzoli", "013", this); //assegnato a slot 6
		this.personale[13] = new PersonaleU2("Diamante", "Calinosi", "014", this); //assegnato a slot 6
		this.personale[14] = new PersonaleU2("Buonaventura", "Braguti", "015", this); //assegnato a slot 7
		this.personale[15] = new PersonaleU2("Valentino", "Finiello", "016", this); //assegnato a slot 7
		this.personale[16] = new PersonaleU2("Fulgenzia", "Manizza", "017", this); //assegnato a slot 8
		this.personale[17] = new PersonaleU2("Dina", "Lampa", "018", this); //assegnato a slot 8
		this.personale[18] = new PersonaleU2("Gioele", "Schranz", "019", this); //assegnato a slot 9
		this.personale[19] = new PersonaleU2("Candido", "Sputon", "020", this); //assegnato a slot 9
		
		this.poolFurgoncini = Executors.newFixedThreadPool(2); //nuovo pool di thread fissato a 2 in quanto abbiamo 2 threads di furgoncino che vengono eseguiti
		this.furgoncini = new FurgoncinoU2 [2];
		this.furgoncini[0] = new FurgoncinoU2("AA000AA", this);
		this.furgoncini[1] = new FurgoncinoU2("BB111BB", this);
		
		magazzinoRitiro = new ArrayList<Pacchetto>();
		magazzinoSpedizioni = new ArrayList<Pacchetto>();
		
		this.ordini = new ArrayList<String>(); //vengono inseriti gli idOrdine dei pacchetti che sono partiti dal supermercato con il furgoncino
		
		this.ordiniConsegnati = new ArrayList<Boolean>();  //quando viene aggiunto un ordine nell'arrayList ordini viene aggiunto, in ordiniConsegnati, 
														   //un booleano (settato inizialmente a false) che comunica se il pacchetto con quell'idOrdine 
														   //sia stato consegnato o meno. In questo modo abbiamo un booleano in posizione "i" relativo 
														   //all'idOrdine in posizione "i" nell'arrayList ordini
		
		this.visible = 0; //viene continuamente aggiornato da OrarioThreadU2 e contiene il numero del primo slot prenotabile in quel determinato momento
		
		this.tabellaSlot = new String[3][10]; //viene creata la matrice contenente gli idOrdine in tabellaSlot[i][j] se in quello slot c'� almeno un cliente che 
											  //lo ha prenotato, altrimenti c'� null. Le righe vanno da 0 a 2 in quanto c'� una coda di al massimo 3 persone per slot,
											  //invece le colonne vanno da 0 a 9 in quanto abbiamo 10 slot massimo:
											  //10:01~10:30|10:31~11:00|11:01~11:30|11:31~12:00|12:01~12:30|12:31~13:00|13:01~13:30|13:31~14:00|14:01~14:30|14:31~15:00|
		
		new OrarioThreadU2(this).start(); //eseguo il thread OrarioThreadU2 che si occupa di controllare l'orario attuale, inizializzare correttamente visible e
										  //aggiornarlo ad ogni cambio di slot
	}

	public synchronized String aggiungiOrdineTabella(int slot, String nome, boolean domiciliato, String indirizzo) { //gli slot vanno da 0 a 9
		boolean fine = false;
		String risp = "";
		
		if(slot < this.getVisible()) {
			risp = "0"; //E' stato indicato uno slot non piu' disponibile!
		}
		else {
			for(int i = 0; i < this.getTabellaSlot().length && !fine; i++) {
				if (this.getTabellaSlot()[i][slot] == null) { //cerca un posto disponibile per lo slot indicato dal cliete (max. posti per slot = 3)
					UUID uuid = UUID.randomUUID();
					this.getTabellaSlot()[i][slot] = uuid.toString();
					fine = true;
					risp = uuid.toString(); //Prenotazione avvenuta con successo! Il tuo id ordine e': idOrdine
					try {
						this.preparaOrdine(uuid.toString(), slot, nome, domiciliato, indirizzo); //Avvia personale di quello slot
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			if (!fine)
				risp = "1"; //E' stato indicato uno slot pieno!
		}
		return risp;		
	}
	
	public synchronized void preparaOrdine(String idOrdine, int slot, String nome, boolean domiciliato, String indirizzo) throws InterruptedException {
		int i = this.accettaOrdine(slot); //i assume il valore dell'indice del primo personale disponibile associato a quello slot nell'array personale 
		  							  	  //i assume valore -1 se nessun personale associato a quello slot � disponibile nell'array personale
	
		while(i == -1) { //rimane in attesa che un personale dello slot si liberi
			wait();
			i = this.accettaOrdine(slot);
		}

		personale[i].setDisponibile(false); //impegna il personale di posizione i
											//idOrdine, domiciliato e indirizzo vengono utilizzati dal personale per creare il pacchetto relativo all'ordine preso in carico
		personale[i].setIdOrdine(UUID.fromString(idOrdine));
		personale[i].setDomiciliato(domiciliato);
		personale[i].setIndirizzo(indirizzo);

		poolPersonale.execute(personale[i]);
	}
	
	
	public int accettaOrdine(int slot) { //cerca nell'array di personale uno disponibile a cui � associato quello slot e ritorna il suo indice oppure -1 se sono tutti occupati
		if(personale[slot*2].disponibile())
			return slot*2;
		else if(personale[slot*2+1].disponibile())
			return slot*2+1;
		else 
			return -1;
	}

	public synchronized void aggiornaVisible() {//metodo chiamato da OrarioThreadU2
		this.visible += 1;
	}
	
	public synchronized String ritornaTabellaSlot() { //ritorna la matrice tabellaSlot serializzata in questo modo:
													  //visible;primaRiga;secondaRiga;terzaRiga dove per ogni cella della matrice uguale a null
													  //viene concatenata la stringa 0 (posizione libera) altrimenti 1 (posizione occupata)
		String s = "";
		if(this.getVisible() == 80 || this.getVisible() == 81) //errore, sono passate le 14:30 oppure il server � stato avviato dopo le 14:30
			s = "e;";
		else
			s = this.getVisible() + ";";
		
		for (int i = 0; i < this.tabellaSlot.length; i++) {
			for (int j = this.getVisible(); j < this.tabellaSlot[i].length; j++) {
				if(this.tabellaSlot[i][j] == null)
					s+= "0";
				else
					s+= "1";
			}			
			s+=";";
		}
		return s.substring(0, s.length()-1); //viene tolto dalla stringa l'ultimo ';' prima di ritornarla dato che � inutile
	}
	
	public synchronized void immagazzina(Pacchetto p) { //inserisce nel magazzino spedizioni o magazzino ritiro il pacchetto a seconda che debba 
														//essere consegnato a domicilio oppure no
		if(p.isDomiciliato())
			this.magazzinoSpedizioni.add(p);
		else
			this.magazzinoRitiro.add(p);
	}
	
	public synchronized void partiFurgoncino() {//fa partire un solo furgoncino oppure tutti e 2 a seconda di quante consegne a domicilio 
												//sono presenti nello slot cosiderato della tabellaSlot
		int numOrdini = 0; //massimo numero di ordini che i furgoncini possono consegnare in un determnato slot
		
		for(int i = 0; i<this.tabellaSlot.length; i++) {
			//se visible � 80 significa che sono passate le 14:31 e il furgoncino deve cercare gli ordini presenti nello slot 9
			if(this.getVisible() == 80 && this.tabellaSlot[i][9] != null) 
				numOrdini += 1;
			//vengono contati quanti ordini ci sono nello slot precedente
			else if(this.getVisible() != 80 && this.tabellaSlot[i][this.getVisible() - 1] != null)
				numOrdini += 1;
		}
		if(!this.magazzinoSpedizioni.isEmpty()) { //se il magazzino spedizioni � vuoto significa che nessuno degli eventuali ordini presenti nello slot
												  // � a domicilio
			
			 //se il quantitativo di ordini � 1 oppure 2 significa che, se dovessero essere ordini a domicilio, un solo furgoncino riuscirebbe a spedirli
			if(numOrdini == 1 || numOrdini == 2)
				poolFurgoncini.execute(furgoncini[0]);
			else if(numOrdini == 3){
				//se il quantitativo di ordini � 3 significa che, se dovessero essere ordini a domicilio, servono tutti e 2 i furgoncini per poterli spedire
				poolFurgoncini.execute(furgoncini[0]);
				poolFurgoncini.execute(furgoncini[1]);
			}
		}
	}
	
	public synchronized boolean caricaPacchetto(FurgoncinoU2 f) throws InterruptedException{//questo metodo viene chiamato da Furgoncino per caricare nel bagagliaio il/i
		Pacchetto p = null;																    //pacchetto/i di un determinato slot
		boolean b = false; //b serve per segnalare che nello slot considerato
						   //� presente almeno un ordine a domicilio (true), false altrimenti
		
		for(int i = 0; !f.isCarico() && i<this.tabellaSlot.length; i++) {
			if(this.getVisible() == 80 && this.tabellaSlot[i][9] != null) {//caso in cui sono passate le 14:30
				p = this.caricaPacchetto(this.tabellaSlot[i][9]); //se l'ordine in posizione tabellaSlot[i][j] non � un ordine a domicilio viene ritornato null
				if (p != null) {
					f.caricaPacchetto(p);
					b = true;
				}
			}
			else if(this.getVisible() != 80 && this.tabellaSlot[i][this.getVisible() - 1] != null) {
				p = this.caricaPacchetto(this.tabellaSlot[i][this.getVisible() - 1]);
				if (p != null) {
					f.caricaPacchetto(p);
					b = true;
				}
			}
		}
		
		return b;
	}
	
	public Pacchetto caricaPacchetto(String idOrdine) {//cerca il pacchetto nel magazzinoSpedizioni
													  //se il pacchetto non viene trovato verr� ritornato null
		Pacchetto p = null;
		boolean fine = false;
		
		for(int i = 0; i<magazzinoSpedizioni.size() && !fine; i++)
			if(magazzinoSpedizioni.get(i).getIdOrdine().toString().equals(idOrdine)) {
				p = magazzinoSpedizioni.get(i);
				magazzinoSpedizioni.remove(i);
				fine = true;
			}
		
		return p;
	}
	
	public synchronized void aggiungiOrdine(String idOrdine) { //viene inserito l'idOrdine nell'arrayList ordini e viene inserito false nella stessa posizione 
															   //in ordiniConsegnati quando il furgoncino parte con il pacchetto
		this.ordini.add(idOrdine);
		this.ordiniConsegnati.add(Boolean.FALSE);
	}
	
	public synchronized void notificaConsegna(String idOrdine) { //setta a true il booleano nella posizione relativa all'idOrdine che viene consegnato
		boolean fine = false;
		
		for (int i = 0; i < ordini.size() && !fine; i++) {
			if(ordini.get(i).equals(idOrdine)) {
				ordiniConsegnati.set(i, Boolean.TRUE);
				fine = true;
			}
		}
	}
	
	public synchronized String ritiraSpesa(String id, int slot) throws InterruptedException { //metodo chiamato dal cliente che controlla se il suo pacchetto � presente nel
		boolean fine = false;	   											      	//magazzinoRitiro, in caso affermativo lo ritira
		String s = "";
		
		if(this.getVisible() - 1 == slot || (this.getVisible() == 80 && slot == 9)) {
			for (int i = 0; i < this.magazzinoRitiro.size() && !fine; i++) {
				if(this.magazzinoRitiro.get(i).getIdOrdine().toString().equals(id)) {
					System.out.println("Il " + this.magazzinoRitiro.get(i).toString() + " e' stato ritirato");
					this.magazzinoRitiro.remove(i);
					fine = true;
					s = "1";
				}
			}
		}else
			s = "0";
		return s;
	}
	
	public synchronized boolean controllaPartenza(String idOrdine) { //viene restituito true se il pacchetto � partito, false altrimenti 
		boolean fine = false;
		
		for (int i = 0; i < ordini.size() && !fine; i++) {
			if(ordini.get(i).equals(idOrdine)) {
				fine = true;
			}
		}
		
		return fine;
	}
	
	public synchronized boolean controllaConsegna(String idOrdine) { //viene restituito true se il pacchetto � stato consegnato, false altrimenti 
		boolean fine = false;
		boolean consegnato = false;
		
		for (int i = 0; i < ordini.size() && !fine; i++) {
			if(ordini.get(i).equals(idOrdine)) {
				consegnato =  ordiniConsegnati.get(i);
				fine = true;
			}
		}
		
		return consegnato;
	}
	
	public synchronized boolean controllaRitiro(String idOrdine, int slot) { //viene restituito true se il pacchetto � pronto per essere ritirato, false altrimenti 
		boolean fine = false;
		
		 //se ci si trova nello slot selezionato (variabile intera slot nella firma del metodo) allora si procede
		 //al controllo del pacchetto nel magazzinoRitiro
		if((this.getVisible() == 80 && slot == 9) || (getVisible() - 1 == slot))
			for (int i = 0; i < magazzinoRitiro.size() && !fine; i++) {
				if(magazzinoRitiro.get(i).getIdOrdine().toString().equals(idOrdine)) {
					fine = true;
				}
			}
		return fine;
	}

	public synchronized void notifica() {
		notifyAll();
	}
	
	//getters e setters
	
	public synchronized int getVisible() {
		return visible;
	}

	public void setVisible(int visible) {
		this.visible = visible;
	}

	public String[][] getTabellaSlot() {
		return tabellaSlot;
	}
}
