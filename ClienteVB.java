import java.util.Random;
import java.util.UUID;

public class ClienteVB extends Thread {
	
	private String nomeUtente;
	private String indirizzo;
	private boolean domiciliato;
	private UUID idOrdine;
	private boolean consegnato;
	private SupermercatoVB supermercato;
	
	public ClienteVB(String nomeUtente, String indirizzo, SupermercatoVB supermercato) {
		this.nomeUtente = nomeUtente;
		this.indirizzo = indirizzo;
		this.domiciliato = false;
		this.supermercato = supermercato;
		this.idOrdine = null;
		this.consegnato = false;
	}
	
	public void run() {
		try {
			if(this.ordina()) { //Al termine del metodo il supermercato fa la execute del personale che si occuper� di impacchettare il suo ordine, 
								 //il metodo ritorna true se viene scelta la consegna a domicilio, false altrimenti
				while(!this.isConsegnato()) {
					Thread.sleep(1000); //Controlla ogni secondo se il pacchetto � stato consegnato dal furgoncino
				}
			}
			else
				while(!this.ritiraOrdine()); //Finche' il metodo ritiraOrdine() non restituisce true, continua a provare a ritirare il pacchetto
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public boolean ordina() throws InterruptedException {
		this.setIdOrdine(UUID.randomUUID()); //Viene generato casualmente un idOrdine
		System.out.println(this.nomeUtente + " ha ordinato la spesa (ordine n." + this.getIdOrdine() + ").");
		
		boolean d = this.domiciliato(); //Viene randomicamente attribuito un valore true o false a d (true --> consegna a casa, 
										//false --> ritiro presso il supermercato)
		
		//nel caso in cui d sia true viene passato l'indirizzo del cliente, in caso contrario no
		if(d)
			this.supermercato.preparaOrdine(this.getIdOrdine(), this.domiciliato, this.indirizzo);
		else
			this.supermercato.preparaOrdine(this.getIdOrdine(), this.domiciliato, null);
		return d;
	}
	
	public boolean domiciliato() {
		Random random = new Random();
		int x = random.nextInt(2 + 1 - 1) + 1; //Viene estratto randomicamente un numero tra 1 e 2, 1 = consegna a domicilio 2 = ritiro presso il supermercato
		
		if(x == 1) { 
			System.out.println(this.nomeUtente + " ha scelto la consegna a domicilio.");
			this.setDomiciliato(true);
		} else 
			System.out.println(this.nomeUtente + " ha scelto il ritiro presso il supermercato.");		
		
		return domiciliato;
	}

	public boolean  ritiraOrdine() throws InterruptedException{
		return supermercato.ritiraSpesa(this.getIdOrdine());
	}
	
	//metodi getters e setters
	private void setDomiciliato(boolean b) {
		this.domiciliato = b;
	}
	public UUID getIdOrdine() {
		return idOrdine;
	}

	public void setIdOrdine(UUID idOrdine) {
		this.idOrdine = idOrdine;
	}

	public boolean isConsegnato() {
		return consegnato;
	}

	public void setConsegnato(boolean consegnato) {
		this.consegnato = consegnato;
	}
}