import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ServerU2 {
	
	private static SupermercatoU2 supermercato = new SupermercatoU2();
	
	public static void startServer() {
		try {
			Set<SelectionKey> keys;
			Iterator<SelectionKey> i;
			Selector selector =  Selector.open();
			SelectionKey key;
			
			ServerSocketChannel server = ServerSocketChannel.open();
			server.bind(new InetSocketAddress("127.0.0.1", 53000));
			
			server.configureBlocking(false); //Server non bloccante
			
			server.register(selector, SelectionKey.OP_ACCEPT);
			
			System.out.println("---------------------------Server---------------------------");
			System.out.println("Running server:" + server.getLocalAddress() + "\n");
			
			while (true) {
				selector.select(); //blocking operation
				
				keys = selector.selectedKeys();
				
				i = keys.iterator();
				
				while (i.hasNext()) {
					key = (SelectionKey) i.next();
					
					i.remove();
					
					if(key.isAcceptable())
						acceptClientRequest (selector, server);
					
					if(key.isReadable())
						readClientBytes(selector, key);
					
					if(key.isValid())
						if(key.isWritable())
							writeResult(key, selector); 
						
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void acceptClientRequest(Selector selector, ServerSocketChannel server) throws IOException {
		SocketChannel client = server.accept();
		
		System.out.println("Nuova connessione dal client: " + client.socket().getPort());
		
		client.configureBlocking(false);
		
		client.register(selector, SelectionKey.OP_WRITE);
		
		return;
	}
	
	public static void readClientBytes(Selector selector, SelectionKey key) throws IOException {
		SocketChannel client = (SocketChannel) key.channel();
		
		int BUFFER_SIZE = 256;
		
		ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
		
		while(client.read(buffer) == 0); //aspetta fin quando non riceve un messaggio dal client
		
		buffer.flip();
		Charset charset = Charset.forName("UTF-8");
		CharsetDecoder decoder = charset.newDecoder();
		CharBuffer charBuffer = decoder.decode(buffer);
		String mess = charBuffer.toString();
		
		if(mess.equals("stop")) { //il client manda "stop" quando vuole chiudere la connessione
			client.close();
		}else if(mess.equals("new")) //il client manda "new" quando vuole effettuare un nuovo ordine e quindi vuole ottenere la tabellaSlot aggiornata
			client.register(selector, SelectionKey.OP_WRITE); 
		else {
			System.out.println("Ordine ricevuto da " + client.socket().getPort() + ": " + "[" + mess + "]");
			
			//il pattern ricevuto dal client � del tipo:
			//slot;nome;1;indirizzo
			//oppure
			//slot;nome;2;null
			//slot >= 0 && slot <= 9
			Matcher m = Pattern.compile("[A-Za-z0-9]+").matcher(mess);
			int slot = -1;
			String nome = "";
			boolean domiciliato = false;
			String indirizzo = "";
			
			if(m.find()) //viene estratto lo slot scelto
				slot = Integer.parseInt(m.group());
			
			if(m.find()) //viene estratto il nomeUtente
				nome = m.group();
			
			if(m.find()) //viene estratta la scelta sul domiciliato
						 // 1: domiciliato = true e 2: domiciliato = false (1 se il cliente ha scelto la consegna a domicilio e 2 se ha scelto il ritiro)
				if(m.group().equals("1"))
					domiciliato = true;
			
			while(m.find()) //viene estratto l'indirizzo
				indirizzo += m.group() + " ";
			
			String risp = supermercato.aggiungiOrdineTabella(slot, nome, domiciliato, indirizzo); //viene aggiunto l'ordine nella tabella nello slot specificato
			
			buffer.clear();
			
			for (int i = 0; i < risp.length(); i++) {
				buffer.put((byte) risp.charAt(i));
			}
			
			buffer.flip();
			client.write(buffer);
			
			if(risp.equals("0") || risp.equals("1")) 												//se viene mandata una striga di errore dal metodo
				client.register(selector, SelectionKey.OP_WRITE);   								//aggiungiOrdineTabella() di supermercato viene
			else																					//rimandata al cliente la tabellaSlot
				new ServerThreadU2(client, risp, domiciliato, slot, supermercato).start();
			
		}
	}
	
	public static void writeResult (SelectionKey key, Selector selector) throws IOException {
		SocketChannel client = (SocketChannel) key.channel();
		
		int BUFFER_SIZE = 128;
		ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
		
		String s = supermercato.ritornaTabellaSlot(); //viene inserita la serializzazione della matrice con gli slot di supermercato in una stringa
		
		for (int i = 0; i < s.length(); i++) {
			buffer.put((byte) s.charAt(i));
		}
		
		buffer.flip();
		client.write(buffer); //viene mandata la stringa contenente la matrice serializzata
		
		System.out.println("Ho mandato: " + s);
		
		client.register(selector, SelectionKey.OP_READ);
	}
}
