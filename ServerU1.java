import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ServerU1 {
	
	private static SupermercatoU1 supermercato = new SupermercatoU1();
	
	public static void startServer() {
		String result = "";
		try {
			Set<SelectionKey> keys;
			Iterator<SelectionKey> i;
			Selector selector =  Selector.open();
			SelectionKey key;
			
			ServerSocketChannel server = ServerSocketChannel.open();
			server.bind(new InetSocketAddress("127.0.0.1", 53000));
			
			server.configureBlocking(false); //Server non bloccante
			
			server.register(selector, SelectionKey.OP_ACCEPT);
			
			System.out.println("--------------------Server--------------------");
			System.out.println("Running server:" + server.getLocalAddress() + "\n");
			
			while (true) {
				selector.select(); //blocking operation
				
				keys = selector.selectedKeys();
				
				i = keys.iterator();
				
				while (i.hasNext()) {
					key = (SelectionKey) i.next();
					
					i.remove();
					
					if(key.isAcceptable())
						acceptClientRequest (selector, server);
					
					if(key.isReadable())
						result = readClientBytes(selector, key);
					
					if (result != null) //perch� viene ritornato null da readClientBytes() quando il client ha chiesto d chiudere la connessione
						if(key.isWritable())
							writeResult(result, key, selector); 
						
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void acceptClientRequest(Selector selector, ServerSocketChannel server) throws IOException {
		SocketChannel client = server.accept();
		
		System.out.println("Nuova connessione dal client: " + client.socket().getPort());
		
		client.configureBlocking(false);
		
		client.register(selector, SelectionKey.OP_READ);
		
		return;
	}
	
	public static String readClientBytes(Selector selector, SelectionKey key) throws IOException {
		SocketChannel client = (SocketChannel) key.channel();
		
		int BUFFER_SIZE = 256;
		
		ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
		
		while(client.read(buffer) == 0); //aspetta fin quando non riceve un messaggio dal client
		
		buffer.flip();
		Charset charset = Charset.forName("UTF-8");
		CharsetDecoder decoder = charset.newDecoder();
		CharBuffer charBuffer = decoder.decode(buffer);
		String mess = charBuffer.toString();
		
		if(mess.equals("stop")) { //il client manda "stop" quando vuole chiudere la connessione
			client.close();
			return null;
		}
		else {
			System.out.println("Ordine ricevuto da " + client.socket().getPort() + ": " + "[" + mess + "]");
			
			//il pattern ricevuto dal client � del tipo:
			//nome;1;indirizzo
			//oppure
			//nome;2;null
			Matcher m = Pattern.compile("[A-Za-z0-9]+").matcher(mess);
			String nome = "";
			boolean domiciliato = false;
			String indirizzo = "";
			
			if(m.find()) //viene estratto il nomeUtente
				nome = m.group();
			
			if(m.find()) //viene estratta la scelta sul domiciliato
						 // 1: domiciliato = true e 2: domiciliato = false (1 se il cliente ha scelto la consegna a domicilio e 2 se ha scelto il ritiro)
				if(m.group().equals("1"))
					domiciliato = true;
			
			while(m.find()) //viene estratto l'indirizzo
				indirizzo += m.group() + " ";
			
			String idOrdine = supermercato.preparaOrdine(nome, domiciliato, indirizzo); //viene preparato l'ordine dal personale con il metodo preparaOrdine() di supermercato
			
			client.register(selector, SelectionKey.OP_WRITE);
			
			return idOrdine + ";" + domiciliato + ";" + indirizzo;
		}
	}
	
	public static void writeResult (String result, SelectionKey key, Selector selector) throws IOException {
		SocketChannel client = (SocketChannel) key.channel();
		String errMsg = "Prenotazione fallita! Tutto il personale e' impegnato a soddisfare altre richieste, riprovare piu' tardi.";
		
		int BUFFER_SIZE = 128;
		ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
		
		//il pattern prodotto dalla readClientBytes() (result) � del tipo:
		//idOrdine;true;indirizzo
		//oppure
		//idOrdine;false;null
		Matcher m = Pattern.compile("[A-Za-z0-9-]+").matcher(result);
		String idOrdine = "";
		boolean domiciliato = false;
		String indirizzo = "";
		
		if(m.find()) //viene estratto l'idOrdine
			idOrdine = m.group();
		
		if(m.find()) //viene estratta la scelta del domiciliato
			if(m.group().equals("true"))
				domiciliato = true;
		
		while(m.find()) //viene estratto l'indirizzo
			indirizzo += m.group() + " ";
		
		String Msg = "Prenotazione presa in carico con successo! Il tuo idOrdine e': " + idOrdine;
		
		if (idOrdine.equals("null")){ //viene caricato il buffer con un messaggio di errore nel caso in cui tutto il personale sia occupato
			for (int i = 0; i < errMsg.length(); i++) {
				buffer.put((byte) errMsg.charAt(i));
			}
			
			buffer.flip();
			client.write(buffer);
		} else {
			for (int i = 0; i < Msg.length(); i++) { //viene caricato il buffer con un messaggio di presa in carico dell'ordine
				buffer.put((byte) Msg.charAt(i));
			}
			
			buffer.flip();
			client.write(buffer);
			
			new ServerThreadU1(client, idOrdine, domiciliato, supermercato).start(); //viene creato e eseguito un thread che si occuper� di gestire i messaggi per aggiornare 
																					 //il client sullo stato del suo pacchetto
		}
		
		client.register(selector, SelectionKey.OP_READ);
	}
}
