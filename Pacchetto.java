import java.util.UUID;

public class Pacchetto {
	private UUID idOrdine;
	private boolean domiciliato;
	private String indirizzo;
	
	public Pacchetto(UUID idOrdine, boolean domiciliato, String indirizzo) {
		super();
		this.idOrdine = idOrdine;
		this.domiciliato = domiciliato;
		this.indirizzo = indirizzo;
	}

	public UUID getIdOrdine() {
		return idOrdine;
	}

	public boolean isDomiciliato() {
		return domiciliato;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	@Override
	public String toString() {
		return "Pacchetto [idOrdine: " + idOrdine + "]";
	}
}
