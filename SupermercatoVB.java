import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SupermercatoVB {
	
	private ExecutorService poolPersonale;
	private ExecutorService poolFurgoncini;
	private PersonaleVB[] personale;
	private FurgoncinoVB[] furgoncini;
	private List<Pacchetto> magazzinoSpedizioni;
	private List<Pacchetto> magazzinoRitiro;
	private List<ClienteVB> clienti;
	
	public SupermercatoVB() {
		//NOTA: il personale impacchetter� gli ordini pi� velocemente di quanto un furgoncino non impieghi a eseguire una consegna
		this.poolPersonale = Executors.newFixedThreadPool(3); //nuovo pool di thread fissato a 3 in quanto al massimo abbiamo 3 threads di personale che vengono eseguiti
        this.personale = new PersonaleVB [3];
		this.personale[0] = new PersonaleVB("Mario", "Rossi", "001", this);
		this.personale[1] = new PersonaleVB("Luigi", "Verdi", "002", this);
		this.personale[2] = new PersonaleVB("Bruno", "Bianchi", "003", this);
		
		this.poolFurgoncini = Executors.newFixedThreadPool(2); //nuovo pool di thread fissato a 2 in quanto abbiamo 2 threads di furgoncino che vengono eseguiti
		this.furgoncini = new FurgoncinoVB [2];
		this.furgoncini[0] = new FurgoncinoVB("AA000AA", this);
		this.furgoncini[1] = new FurgoncinoVB("BB111BB", this);
		poolFurgoncini.execute(furgoncini[0]); //vengono eseguiti i 2 furgoncini, questi saranno sempre attivi lungo tutta l'esecuzione del programma in attesa che il
											   //magazzino spedizioni venga riempito
		poolFurgoncini.execute(furgoncini[1]);
		
		magazzinoRitiro = new ArrayList<Pacchetto>();
		magazzinoSpedizioni = new ArrayList<Pacchetto>();
		clienti = new ArrayList<ClienteVB>();
	}
	
	public synchronized void preparaOrdine(UUID uuid, boolean domiciliato, String indirizzo) throws InterruptedException { //chiamato da cliente al momento dell'ordine
		int i = this.accettaOrdine(); //i assume il valore dell'indice del primo personale disponibile nell'array personale
									  // i assume valore -1 se nessun personale � disponibile nell'array personale
		while(i == -1) { //rimane in attesa che un personale si liberi
			wait();
			i = this.accettaOrdine();
		}

		personale[i].setDisponibile(false); //impegna il personale di posizione i
		//uuid, domiciliato e indirizzo vengono utilizzati dal personale per creare il pacchetto relativo all'ordine preso in carico
		personale[i].setIdOrdine(uuid);
		personale[i].setDomiciliato(domiciliato);
		personale[i].setIndirizzo(indirizzo);
		
		poolPersonale.execute(personale[i]);
	}
	
	public int accettaOrdine() { //scorre l'array di personale cercandone uno disponibile ritornando il suo indice, ritorna -1 se sono tutti occupati
		for (int i = 0; i < personale.length; i++) {
			if (personale[i].disponibile()) {
				return i;
			}
		}
		return -1;
	}
	
	public synchronized void notifica() {
		notifyAll();
	}
	
	public synchronized void immagazzina(Pacchetto p) { //inserisce nel magazzino spedizioni o magazzino ritiro il pacchetto a seconda che debba 
														//essere consegnato a domicilio oppure no
		if(p.isDomiciliato())
			this.magazzinoSpedizioni.add(p);
		else
			this.magazzinoRitiro.add(p);
	}
	
	public synchronized void caricaPacchetto(FurgoncinoVB f) throws InterruptedException{//questo metodo viene chiamato da Furgoncino per caricare nel bagagliaio il/i
																					   //pacchetto/i presenti in magazzinoSpedizioni
		while(this.magazzinoSpedizioni.isEmpty()) //se il magazzino � vuoto aspetta che qualcuno lo riempa
			wait();
		
		while(!f.isCarico() && !this.magazzinoSpedizioni.isEmpty()) { //la seconda condizione serve se il furgoncino trova un solo pacchetto nel magazzino
			f.caricaPacchetto(this.magazzinoSpedizioni.get(this.magazzinoSpedizioni.size()-1));
			this.magazzinoSpedizioni.remove(this.magazzinoSpedizioni.size()-1);
		}
	}
	
	public synchronized boolean ritiraSpesa(UUID id) throws InterruptedException { //metodo chiamato dal cliente che controlla se il suo pacchetto � presente nel
																				   //magazzinoRitiro, in caso affermativo lo ritira, altrimenti richiamer� il metodo
																				   //fino a quando il suo pacchetto non sar� pronto
		for (int i = 0; i < this.magazzinoRitiro.size(); i++) {
			if(this.magazzinoRitiro.get(i).getIdOrdine().compareTo(id) == 0) {
				System.out.println("Il " + this.magazzinoRitiro.get(i).toString() + " e' stato ritirato");
				this.magazzinoRitiro.remove(i);
				return true;
			}
		}
		return false;
	}
	
	//getters
	public ExecutorService getPoolPersonale() {
		return poolPersonale;
	}

	public ExecutorService getPoolFurgoncini() {
		return poolFurgoncini;
	}

	public List<ClienteVB> getClienti() {
		return clienti;
	}
}