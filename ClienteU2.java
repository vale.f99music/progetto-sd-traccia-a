import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class ClienteU2 {
	
	private static Scanner tastiera = new Scanner(System.in);
	
	public static void startClient() {
		Socket socket;
		InetAddress serverAddress;
		int serverPort;
		String serverResponse = "";
		String command = "";
		String nomeUtente = "";	
		boolean domiciliato = false;
		PrintWriter out;
		char risp = 'n';
		int visibile = -1;
		
		System.out.println("--------------------------Cliente---------------------------");
		
		try {
			serverAddress = InetAddress.getByName("localhost");
			serverPort = 53000;
			socket = new Socket(serverAddress, serverPort);
			System.out.println("Connected to: " + socket.getInetAddress());
			BufferedInputStream in = new BufferedInputStream (socket.getInputStream());
			out = new PrintWriter(socket.getOutputStream());
			
			
			System.out.println("Benvenuto nel sistema ConsegnaSpesa24!");
			System.out.println("Il servizio chiude alle ore 15:00");
			
			do {
				System.out.print("Inserire il nomeUtente (non deve contenere spazi): ");
				nomeUtente = tastiera.nextLine();
			}while(controllaNomeUtente(nomeUtente)); //se il nome utente contiene spazi viene richiesto di reinserirlo
			
			do {
				do {
					visibile = stampaTabella(in); //viene stampata la tabella con gli slot disponibili e selezionati a seconda dell'orario in cui ci si trova
					
					if(visibile == 14) { //se viene ritornato 14 � perch� il server ha segnalato che il cliente si � connesso dopo le 14:31 che � l'orario in cui
						   				 //il servizio chiude e quindi viene chiusa la connessione 
						out.print("stop"); //viene inviato "stop" al server per avvisarlo che si vuole chiudere la connessione
						out.flush();
						socket.close();
						System.out.println("Connessione chiusa");
						risp = 'n'; //imposta risp a n per uscire dal do while
					}else {
						do {
							System.out.println("Inserire uno slot da prenotare (Es: Slot4 --inserire--> 4): ");
							command = tastiera.nextLine();
						}while(Integer.parseInt(command) < visibile || Integer.parseInt(command) > 9); //se il numero non � tra il 9 e il primo slot disponibile viene
																									   //chiesto di reinserire lo slot che si vuole scegliere
						
						command += ";" + menu(nomeUtente); //il metodo menu() stampa a schermo il men� e memorizza le scelte del cliente in una stringa (command) 
														   //che viene concatenata allo slot scelto in precedenza, dopo aver eseguito questa istruzione command conterr�
														   //una stringa del tipo: slot;nome;1;indirizzo oppure slot;nome;2;null
						
						domiciliato = estraiScelta(command.substring(2)); //viene estratta la scelta del domiciliato che viene salvata in un booleano (1 = true, 2 = false)
						
						out.print(command); // viene mandato un messaggio contenente le scelte del cliente al server
						out.flush();
						System.out.println("Ordine inviato al server");
		
						serverResponse = readFromServer(in); //viene ricevuta risposta del server
						
						//se vengono ricevute stringhe del tipo: "0" oppure "1" viene stampato un messaggio di errore a seconda dell'errore, altrimenti viene
						//ricevuto l'idOrdine assegnato al cliente
						if(serverResponse.equals("0"))
							System.out.println("E' stato indicato uno slot non piu' disponibile!");
						else if (serverResponse.equals("1")) {
							System.out.println("E' stato indicato uno slot pieno!");
							if(visibile == 9) {
								out.print("stop"); //viene inviato "stop" al server per avvisarlo che si vuole chiudere la connessione
								out.flush();
								socket.close();
								System.out.println("Connessione chiusa");
							}
						}
						else
							System.out.println("Prenotazione avvenuta con successo! Il tuo id ordine e': " + serverResponse);
					}
				}while((serverResponse.equals("0") || serverResponse.equals("1")) && visibile != 9);
				//finch� la risposta del server � una risposta di errore viene ristampata la tabella e viene richiesto al cliente di reinserire i dati e lo slot scelto
				
				if(!socket.isClosed()) { //se la socket non � stata chiusa in precedenza ci si aspettano risposte diverse a seconda del valore di domiciliato
					if (domiciliato) {
						System.out.println("Il tuo ordine partira' nell' orario selezionato. Attendere prego...");
						serverResponse = readFromServer(in); //viene ricevuto un messaggio che informa il cliente dell'avvenuta partenza del pacchetto dal supermercato
						System.out.println(serverResponse);
						
						serverResponse = readFromServer(in); //viene ricevuto un messaggio che informa il cliente dell'avvenuta consegna
						System.out.println(serverResponse);
					}else {
						System.out.println("Potra' andare a ritirare il suo ordine dall' orario selezionato. Attendere prego...");
						serverResponse = readFromServer(in); //viene ricevuto un messaggio che informa il cliente che il pacchetto � pronto per il ritiro
						
						do {
							System.out.println(serverResponse);
							System.out.println("Vuoi ritirare l'ordine? [y/n]");
							risp = tastiera.nextLine().charAt(0);
						}while(risp != 'y' && risp != 'Y'); //finch� il cliente non risponde con 'Y' oppure 'y' viene stampata di nuovo la domanda
						
						out.print("ok"); //se il cliente risponde di si viene mandato un messaggio al server per avvisarlo che il cliente ritira il pacchetto
						out.flush();
						
						serverResponse = readFromServer(in); //riceve l'idOrdine del pacchetto che ha ritirato
						
						System.out.println("Pacchetto " + serverResponse + " ritirato");
					}
					
					System.out.println("Vuoi effettuare un nuovo ordine? [y/n]");
					risp = tastiera.nextLine().charAt(0);
					
					if(risp == 'Y' || risp == 'y') { //in caso affermativo viene inviato al server una stringa che va ad indicare che si vuole ricevere
													 //la tabella aggiornata
						out.print("new");
						out.flush();
					}
					
				}
			}while(risp == 'y' || risp == 'Y'); //se il cliente vuole effettuare un nuovo ordine viene rieseguito il codice dalla scelta dello slot in poi
			
			if(!socket.isClosed()) { //se il cliente decide di non voler effettuare un nuovo ordine viene mandato al server un messaggio per avvisarlo 
									 //che vuole chiudere la connessione se non � gi� stata chiusa in precedenza (la connessione pu� essere gi� stata
									 //chiusa se ci si collega ad un orario oltre le 14:31)
				out.print("stop");
				out.flush();
				socket.close();
				System.out.println("Connessione chiusa");
				System.out.println("Arrivederci e grazie per aver usufruito del servizio ConsegnaSpesa24!");
			}
			
		}catch(IOException e) {
			e.printStackTrace();
		}		
	}
	
	public static int stampaTabella(BufferedInputStream in) throws IOException {
		boolean orarioOk = false;
		String serverResponse;
		
		serverResponse = readFromServer(in); //La risposta sar� del tipo "visible;prima riga;seconda riga;terza riga della matrice nel server"
		
		int visible = Character.getNumericValue(serverResponse.charAt(0)); //viene estratto dalla serverResponse il numero del primo slot visibile a seconda dell'orario
																		   //(visibile >= 0 && visibile <= 9) oppure visibile == 14 se � chiuso a quell'ora
		serverResponse = serverResponse.substring(2); //rimuove il valore visible da serverResponse (dato che � stato salvato), quindi ora la serverResponse � composta da: 
													  //primaRiga;secondaRiga;terzaRiga della matrice nel server
		
		switch(visible){ //a seconda del primo slot visibile viene stampata l'intestazione della tabella
		case 0:
			System.out.println("||   Slot0   |   Slot1   |   Slot2   |   Slot3   |   Slot4   |   Slot5   |   Slot6   |   Slot7   |   Slot8   |   Slot9   ||");
			System.out.println("---------------------------------------------------------------------------------------------------------------------------");
			System.out.println("||10:01~10:30|10:31~11:00|11:01~11:30|11:31~12:00|12:01~12:30|12:31~13:00|13:01~13:30|13:31~14:00|14:01~14:30|14:31~15:00||");
			orarioOk = true;
			break;
		case 1:
			System.out.println("||   Slot1   |   Slot2   |   Slot3   |   Slot4   |   Slot5   |   Slot6   |   Slot7   |   Slot8   |   Slot9   ||");
			System.out.println("---------------------------------------------------------------------------------------------------------------");
			System.out.println("||10:31~11:00|11:01~11:30|11:31~12:00|12:01~12:30|12:31~13:00|13:01~13:30|13:31~14:00|14:01~14:30|14:31~15:00||");
			orarioOk = true;
			break;
		case 2:
			System.out.println("||   Slot2   |   Slot3   |   Slot4   |   Slot5   |   Slot6   |   Slot7   |   Slot8   |   Slot9   ||");
			System.out.println("---------------------------------------------------------------------------------------------------");
			System.out.println("||11:01~11:30|11:31~12:00|12:01~12:30|12:31~13:00|13:01~13:30|13:31~14:00|14:01~14:30|14:31~15:00||");
			orarioOk = true;
			break;
		case 3:
			System.out.println("||   Slot3   |   Slot4   |   Slot5   |   Slot6   |   Slot7   |   Slot8   |   Slot9   ||");
			System.out.println("---------------------------------------------------------------------------------------");
			System.out.println("||11:31~12:00|12:01~12:30|12:31~13:00|13:01~13:30|13:31~14:00|14:01~14:30|14:31~15:00||");
			orarioOk = true;
			break;
		case 4:
			System.out.println("||   Slot4   |   Slot5   |   Slot6   |   Slot7   |   Slot8   |   Slot9   ||");
			System.out.println("---------------------------------------------------------------------------");
			System.out.println("||12:01~12:30|12:31~13:00|13:01~13:30|13:31~14:00|14:01~14:30|14:31~15:00||");
			orarioOk = true;
			break;
		case 5:
			System.out.println("||   Slot5   |   Slot6   |   Slot7   |   Slot8   |   Slot9   ||");
			System.out.println("---------------------------------------------------------------");
			System.out.println("||12:31~13:00|13:01~13:30|13:31~14:00|14:01~14:30|14:31~15:00||");
			orarioOk = true;
			break;
		case 6:
			System.out.println("||   Slot6   |   Slot7   |   Slot8   |   Slot9   ||");
			System.out.println("---------------------------------------------------");
			System.out.println("||13:01~13:30|13:31~14:00|14:01~14:30|14:31~15:00||");
			orarioOk = true;
			break;
		case 7:
			System.out.println("||   Slot7   |   Slot8   |   Slot9   ||");
			System.out.println("---------------------------------------");
			System.out.println("||13:31~14:00|14:01~14:30|14:31~15:00||");
			orarioOk = true;
			break;
		case 8:
			System.out.println("||   Slot8   |   Slot9   ||");
			System.out.println("---------------------------");
			System.out.println("||14:01~14:30|14:31~15:00||");
			orarioOk = true;
			break;
		case 9:
			System.out.println("||   Slot9   ||");
			System.out.println("---------------");
			System.out.println("||14:31~15:00||");
			orarioOk = true;
			break;
		default: //se visibile ha un numero diverso dai casi sopra significa che il server vuole segnalare che il servizio � chiuso a quell'ora
			System.out.println("Siamo spiacenti ma il servizio non � aperto a quest'ora, si prega di riprovare domani.");
		}
		
		if(orarioOk) { //se il valore di visibile rientra in uno dei casi sopra e non nel default orarioOk viene settato a true e quindi viene stampata la stringa
					   //ricevuta dal server contenente la matrice tabellaSlot deserializzata e formattata in maniera pi� leggibile
			for (int i = 0; i < serverResponse.length(); i++) {
				System.out.print("||");
				while (i < serverResponse.length() && serverResponse.charAt(i) != ';') {
					if(serverResponse.charAt(i) == '0')
						System.out.print("           |");
					if(serverResponse.charAt(i) == '1')
						System.out.print("     x     |");
					i++;
				}
				System.out.print("| " + "\n");
			}
		}
		
		switch(visible){ //a seconda del valore di visible viene stampata una linea di trattini di diversa lunghezza per poter chiudere la tabella 
		case 0:
			System.out.println("---------------------------------------------------------------------------------------------------------------------------");
			break;
		case 1:
			System.out.println("---------------------------------------------------------------------------------------------------------------");
			break;
		case 2:
			System.out.println("---------------------------------------------------------------------------------------------------");
			break;
		case 3:
			System.out.println("---------------------------------------------------------------------------------------");
			break;
		case 4:
			System.out.println("---------------------------------------------------------------------------");
			break;
		case 5:
			System.out.println("---------------------------------------------------------------");
			break;
		case 6:
			System.out.println("---------------------------------------------------");
			break;
		case 7:
			System.out.println("---------------------------------------");
			break;
		case 8:
			System.out.println("---------------------------");
			break;
		case 9:
			System.out.println("---------------");
			break;
		}
		return visible;
	}

	public static String menu(String nomeUtente) { //stampa a video il menu con le scelte e i dati da inserire e memorizza le risposte in command
		String command = "";		
	
		System.out.println("Specificare se si preferisce:");
		do{
			command = "";
			System.out.println("[1] Consegna a domicilio");
			System.out.println("[2] Ritiro presso il supermercato");
			command = tastiera.nextLine() + ";";
		}while(!command.equals("1;")&&!command.equals("2;")); //se viene scritto qualcosa di diverso da 1 oppure 2 vengono stampate di nuovo le due scelte finch� il cliente
															  //non scrive uno dei due numeri accettati
		
		if(command.equals("1;")){ //solo se il cliente ha scelto la consegna a domicilio viene richiesto l'indirizzo senn� viene concatenata la stringa "null" 
								  //al posto dell'indirizzo
			System.out.println("Inserire l'indirizzo: ");
			command += tastiera.nextLine();
		}else{
			command += "null";
		}
				
		command = nomeUtente + ";" + command;
		
		return command;
	}
	
	public static boolean controllaNomeUtente(String nomeUtente) { //viene controllata la sintassi del nome utente (non deve contenere spazi) in caso contrario
																   //viene stampato a video un messaggio di errore
		boolean err = false;
		for(int i = 0; i < nomeUtente.length() && !err; i++) {
			if (nomeUtente.charAt(i) == ' ') {
				System.out.println("Sintassi nome utente errata! Inserire un altro nomeUtente.");
				err = true;
			}
		}
		return err;
	}
	
	public static String readFromServer(BufferedInputStream in) throws IOException { //vengono letti i bytes ricevuti dal server
		String s = "";
		int x = 0;
		byte[] buffer =  new byte[128];
		x = in.read(buffer);
		
		buffer[x] = (byte) '\n';
		
		for (int i = 0; buffer[i] != (byte) '\n'; i++) {
			s += (char) buffer[i];
		}
		
		return s;
	}
	
	public static boolean estraiScelta(String cmd) { //viene ricavata la scelta del domiciliato dalla stringa passata
		boolean fine = false;
		char sceltaDomicilio = ' ';
		for (int i = 0; i < cmd.length() && !fine; i++) {
			if (cmd.charAt(i) == ';') {
				sceltaDomicilio = cmd.charAt(i+1);
				fine = true;
			}
		}
		
		if (sceltaDomicilio == '1')
			return true;
		else
			return false;
	}
}