import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SupermercatoU1 {
	
	private ExecutorService poolPersonale;
	private ExecutorService poolFurgoncini;
	private PersonaleU1[] personale;
	private FurgoncinoU1[] furgoncini;
	private List<Pacchetto> magazzinoSpedizioni;
	private List<Pacchetto> magazzinoRitiro;
	private List<String> ordini;
	private List<Boolean> ordiniConsegnati;
	
	public SupermercatoU1() {
		this.poolPersonale = Executors.newFixedThreadPool(3); //nuovo pool di thread fissato a 3 in quanto al massimo abbiamo 3 threads di personale che vengono eseguiti
        this.personale = new PersonaleU1 [3];
		this.personale[0] = new PersonaleU1("Mario", "Rossi", "001", this);
		this.personale[1] = new PersonaleU1("Luigi", "Verdi", "002", this);
		this.personale[2] = new PersonaleU1("Bruno", "Bianchi", "003", this);
		
		this.poolFurgoncini = Executors.newFixedThreadPool(2); //nuovo pool di thread fissato a 2 in quanto abbiamo 2 threads di furgoncino che vengono eseguiti
		this.furgoncini = new FurgoncinoU1 [2];
		this.furgoncini[0] = new FurgoncinoU1("AA000AA", this);
		this.furgoncini[1] = new FurgoncinoU1("BB111BB", this);
		
		magazzinoRitiro = new ArrayList<Pacchetto>();
		magazzinoSpedizioni = new ArrayList<Pacchetto>();
		
		this.ordini = new ArrayList<String>(); //vengono inseriti gli idOrdine dei pacchetti che sono partiti dal supermercato con il furgoncino
		this.ordiniConsegnati = new ArrayList<Boolean>();  //quando viene aggiunto un ordine nell'arrayList ordini viene aggiunto, in ordiniConsegnati, 
														   //un booleano (settato inizialmente a false) che comunica se il pacchetto con quell'idOrdine 
														   //sia stato consegnato o meno. In questo modo abbiamo un booleano in posizione "i" relativo 
														   //all'idOrdine in posizione "i" nell'arrayList ordini
	}
	
	public synchronized String preparaOrdine(String nome, boolean domiciliato, String indirizzo) {
		int i = this.accettaOrdine(); //i assume il valore dell'indice del primo personale disponibile nell'array personale
		  							  //i assume valore -1 se nessun personale � disponibile nell'array personale
		
		if(i == -1)
			return "null";
		else {
			UUID uuid = UUID.randomUUID(); //viene generato l'idOrdine
			personale[i].setDisponibile(false); //impegna il personale di posizione i
			
			//uuid, domiciliato e indirizzo vengono utilizzati dal personale per creare il pacchetto relativo all'ordine preso in carico
			personale[i].setIdOrdine(uuid);
			personale[i].setDomiciliato(domiciliato);
			personale[i].setIndirizzo(indirizzo);
			
			poolPersonale.execute(personale[i]);
			return uuid.toString();
		}
	}
	
	public int accettaOrdine() { //scorre l'array di personale cercandone uno disponibile ritornando il suo indice, ritorna -1 se sono tutti occupati
		for (int i = 0; i < personale.length; i++) {
			if (personale[i].disponibile()) {
				return i;
			}
		}
		return -1;
	}
	
	public synchronized void notifica() {
		notifyAll();
	}
	
	public synchronized void immagazzina(Pacchetto p) { //inserisce nel magazzino spedizioni o magazzino ritiro il pacchetto a seconda che debba 
														  //essere consegnato a domicilio oppure no
		if(p.isDomiciliato())
			this.magazzinoSpedizioni.add(p);
		else
			this.magazzinoRitiro.add(p);
	}
	
	public synchronized void partiFurgoncino() { 
		int i = this.furgoncinoDisponibile();
		
		while(i == -1) {
			try {
				wait(); //aspetta che si liberi un furgoncino
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			i = this.furgoncinoDisponibile();
		}
		
		furgoncini[i].setDisponibile(false);
		poolFurgoncini.execute(furgoncini[i]);
	}
	
	public int furgoncinoDisponibile() { //scorre l'array di furgoncini cercandone uno disponibile ritornando il suo indice, ritorna -1 se sono tutti occupati
		for (int i = 0; i < furgoncini.length; i++) {
			if (furgoncini[i].isDisponibile()) {
				return i;
			}
		}
		return -1;
	}
	
	public synchronized void caricaPacchetto(FurgoncinoU1 f) throws InterruptedException{//questo metodo viene chiamato da Furgoncino per caricare nel bagagliaio il/i
																						 //pacchetto/i presenti in magazzinoSpedizioni
		while(this.magazzinoSpedizioni.isEmpty()) //se il magazzino � vuoto aspetta che qualcuno lo riempa
			wait();
		
		while(!f.isCarico() && !this.magazzinoSpedizioni.isEmpty()) { //la seconda condizione serve se il furgoncino trova un solo pacchetto nel magazzino
			f.caricaPacchetto(this.magazzinoSpedizioni.get(this.magazzinoSpedizioni.size()-1));
			this.magazzinoSpedizioni.remove(this.magazzinoSpedizioni.size()-1);
		}
	}
	
	public synchronized void ritiraSpesa(String id) throws InterruptedException { //metodo chiamato dal cliente che controlla se il suo pacchetto � presente nel
		boolean fine = false;	   											      //magazzinoRitiro, in caso affermativo lo ritira
		for (int i = 0; i < this.magazzinoRitiro.size() && !fine; i++) {
			if(this.magazzinoRitiro.get(i).getIdOrdine().toString().equals(id)) {
				System.out.println("Il " + this.magazzinoRitiro.get(i).toString() + " e' stato ritirato");
				this.magazzinoRitiro.remove(i);
				fine = true;
			}
		}
	}
	
	public synchronized void aggiungiOrdine(String idOrdine) { //viene inserito l'idOrdine nell'arrayList ordini e viene inserito false nella stessa posizione 
															   //in ordiniConsegnati quando il furgoncino parte con il pacchetto
		this.ordini.add(idOrdine);
		this.ordiniConsegnati.add(Boolean.FALSE);
	}
	
	public synchronized void notificaConsegna(String idOrdine) { //setta a true il booleano nella posizione relativa all'idOrdine che viene consegnato
		boolean fine = false;
		
		for (int i = 0; i < ordini.size() && !fine; i++) {
			if(ordini.get(i).equals(idOrdine)) {
				ordiniConsegnati.set(i, Boolean.TRUE);
				fine = true;
			}
		}
	}
	
	public synchronized boolean controllaPartenza(String idOrdine) { //viene restituito true se il pacchetto � partito, false altrimenti 
		boolean fine = false;
		
		for (int i = 0; i < ordini.size() && !fine; i++) {
			if(ordini.get(i).equals(idOrdine)) {
				fine = true;
			}
		}
		
		return fine;
	}
	
	public synchronized boolean controllaConsegna(String idOrdine) { //viene restituito true se il pacchetto � stato consegnato, false altrimenti 
		boolean fine = false;
		boolean consegnato = false;
		
		for (int i = 0; i < ordini.size() && !fine; i++) {
			if(ordini.get(i).equals(idOrdine)) {
				consegnato =  ordiniConsegnati.get(i);
				fine = true;
			}
		}
		
		return consegnato;
	}
	
	public synchronized boolean controllaRitiro(String idOrdine) { //viene restituito true se il pacchetto � pronto per essere ritirato, false altrimenti 
	boolean fine = false;
		
		for (int i = 0; i < magazzinoRitiro.size() && !fine; i++) {
			if(magazzinoRitiro.get(i).getIdOrdine().toString().equals(idOrdine)) {
				fine = true;
			}
		}
		
		return fine;
	}
}
