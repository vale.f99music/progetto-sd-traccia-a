import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

public class ServerThreadU2 extends Thread{
	
	private SocketChannel client;
	private String idOrdine;
	private boolean domiciliato;
	private SupermercatoU2 s;
	private int slot;
	
	public ServerThreadU2(SocketChannel channel, String idOrdine, boolean domiciliato, int slot, SupermercatoU2 supermercato) {
		this.client = channel;
		this.idOrdine = idOrdine;
		this.domiciliato = domiciliato;
		this.s = supermercato;
		this.slot = slot;
	}
	
	public void run() {
		int BUFFER_SIZE = 128;
		ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
		
		try {
			
			if(this.domiciliato) {
				String msgPartenza = "Il tuo pacchetto e' stato spedito";
				String msgConsegna = "Il tuo pacchetto e' stato consegnato";
				
				while(!s.controllaPartenza(this.idOrdine)) //finch� il furgoncino non � partito continua ad eseguire il ciclo dopo aver fatto una Thread.sleep(15000)
					Thread.sleep(4000);
				
				for (int i = 0; i < msgPartenza.length(); i++) //quando il furgoncino parte viene caricato sul buffer un messaggio di partenza
					buffer.put((byte) msgPartenza.charAt(i));
				
				buffer.flip();
				client.write(buffer);//viene scritto al client che il pacchetto � stato spedito
				
				buffer.clear();
				
				while(!s.controllaConsegna(idOrdine)) //finch� il furgoncino non ha consegnato il pacchetto continua ad eseguire il ciclo dopo aver fatto una Thread.sleep(500)
					Thread.sleep(500);
				
				for (int i = 0; i < msgConsegna.length(); i++) //quando il furgoncino consegna il pacchetto viene caricato sul buffer un messaggio di avvenuta consegna
					buffer.put((byte) msgConsegna.charAt(i));
				
				buffer.flip();
				client.write(buffer);//viene scritto al client che il furgoncino ha consegnato il pacchetto
			} else {
				String msgRitiro = "Il tuo pacchetto e' pronto per essere ritirato. (ritirare entro il termine dello slot specificato: Slot" + slot + ")";
				
				while(!s.controllaRitiro(idOrdine, slot)) //finch� non ci si trova nello slot temporale corretto viene fatto Thread.sleep(500)
					Thread.sleep(500);
			
				for (int i = 0; i < msgRitiro.length(); i++) //quando il pacchetto � pronto per essere ritirato viene caricato sul buffer un messaggio di possibilit� di ritiro
					buffer.put((byte) msgRitiro.charAt(i));
				
				buffer.flip();
				client.write(buffer); //viene scritto al client che il pacchetto � pronto per essere ritirato
				buffer.clear();
				
				while(client.read(buffer) == 0); //in attesa della risposta per ritiro
				
				buffer.clear();
				
				if(s.ritiraSpesa(idOrdine, slot).equals("1")) { //viene ritirata la spesa dal cliente
					for (int i = 0; i < idOrdine.length(); i++) 
						buffer.put((byte) idOrdine.charAt(i)); 
				}else {
					String err = "non piu' disponibile per essere";
					for (int i = 0; i < err.length(); i++) 
						buffer.put((byte) err.charAt(i)); 
				}
				
				buffer.flip();
				client.write(buffer); //viene inviato al client l'idOrdine del pacchetto che ha ritirato
				
			}
		}catch(IOException e) {
			e.printStackTrace();
		}catch(InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}