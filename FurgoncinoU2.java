import java.util.ArrayList;
import java.util.List;

public class FurgoncinoU2 extends Thread{
	private String targa;
	private SupermercatoU2 s;
	private boolean carico;
	private List<Pacchetto> Bagagliaio;
	
	public FurgoncinoU2(String targa, SupermercatoU2 s) {
		this.targa = targa;
		this.s = s;
		this.carico = false;
		this.Bagagliaio = new ArrayList<Pacchetto>();
	}
	public void run() {
		try {
			//carica il pacchetto dal magazzino del supermercato al bagagliaio attraverso caricaPacchetto() di supermercato che restituisce true
			//se ci sono pacchetti da caricare e effettua la consegna
			if(s.caricaPacchetto(this)) { 
				this.consegnaSpesa(); 
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
	}
	public void consegnaSpesa() throws InterruptedException { 
		String s = "Il furgoncino (targa: " + this.targa + ") e' partito dal supermercato con " + this.Bagagliaio.size() + " ordine/i: ";
		
		for (int i = 0; i < Bagagliaio.size(); i++) {
			this.s.aggiungiOrdine(this.Bagagliaio.get(i).getIdOrdine().toString()); //aggiunge l'ordine all'arrayList ordini di supermercato
			s += this.Bagagliaio.get(i).toString() + " ";
		}
		System.out.println(s); //stampa messaggio di partenza
		
		for (int i = 0; i < Bagagliaio.size(); i++) {
			Thread.sleep((int)(Math.random() * 60000)); //consegna la spesa a domicilio
			this.s.notificaConsegna(this.Bagagliaio.get(i).getIdOrdine().toString()); //setta a true il booleano nell'arrayList ordiniConsegna di supermercato
			System.out.println("Il furgoncino (targa: " + this.targa + ") ha consegnato la spesa (ordine: " + this.Bagagliaio.get(i).getIdOrdine() + ") all'indirizzo: " + this.Bagagliaio.get(i).getIndirizzo());
		}
		this.Bagagliaio.clear();
		this.setCarico(false);
	}
	
	public void caricaPacchetto (Pacchetto p) { //viene aggiunto il pacchetto p al bagagliaio (la dimensione massima del bagagliaio � di 2 pacchetti)
		this.Bagagliaio.add(p);
		if(Bagagliaio.size() == 2)
			this.setCarico(true);
	}
	
	//setters e getters
	public void setCarico(boolean carico) {
		this.carico = carico;
	}
	
	public boolean isCarico() {
		return carico;
	}
}
