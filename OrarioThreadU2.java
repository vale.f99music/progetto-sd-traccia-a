
public class OrarioThreadU2 extends Thread{
	
	private SupermercatoU2 s;
	
	public OrarioThreadU2(SupermercatoU2 supermercato) {
		this.s = supermercato;
	}
	
	public void run() {
		try {
			impostaVisible(); //questo metodo imposta il valore di visible a seconda dell'orario in cui viene istanziata la classe supermercato
			//viene controllato l'orario e viene aggiornato man mano il valore di visible quando necessario
			while(this.s.getVisible() <= 9 && (java.time.LocalDateTime.now().getHour() < 14 || (java.time.LocalDateTime.now().getHour() == 14 && java.time.LocalDateTime.now().getMinute() < 31))) {
				if(java.time.LocalDateTime.now().getHour() >= 10 && (java.time.LocalDateTime.now().getMinute() == 01 || java.time.LocalDateTime.now().getMinute() == 31)) {
					if(this.s.getVisible() < 9) {
						this.s.aggiornaVisible();
						Thread.sleep(35000);
						s.partiFurgoncino();
						Thread.sleep(26000);
					}
				}
				else {
					Thread.sleep(800);
				}
			}
		
			this.s.setVisible(80); //viene impostato ad 80 visible quando l'orario � oltre le 14:31
			Thread.sleep(35000);
			s.partiFurgoncino();
			while (java.time.LocalDateTime.now().getHour() < 15 || java.time.LocalDateTime.now().getMinute() < 01)
				Thread.sleep(60000);
			s.setVisible(81);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void impostaVisible() {
		int ora = java.time.LocalDateTime.now().getHour();
		int minuto = java.time.LocalDateTime.now().getMinute();
		
		if ((ora == 14 && minuto > 30) || ora >= 15)
			this.s.setVisible(80); //imposta un numero che indica un errore in quanto il servizio non � pi� disponibile
		else {
			switch(ora) {
			//viene settato il valore di visible in base all'ora e successivamente anche al valore del minuto (in un'ora abbiamo 2 slot)
			case 10:
				this.s.setVisible(0);
				if (minuto >= 1 && minuto <= 30)
					this.s.aggiornaVisible();
				else if (minuto > 30){
					this.s.aggiornaVisible();
					this.s.aggiornaVisible();
				}
				break;
			case 11:
				this.s.setVisible(2);
				if (minuto >= 1 && minuto <= 30)
					this.s.aggiornaVisible();
				else if (minuto > 30){
					this.s.aggiornaVisible();
					this.s.aggiornaVisible();
				}
				break;
			case 12:
				this.s.setVisible(4);
				if (minuto >= 1 && minuto <= 30)
					this.s.aggiornaVisible();
				else if (minuto > 30){
					this.s.aggiornaVisible();
					this.s.aggiornaVisible();
				}
				break;
			case 13:
				this.s.setVisible(6);
				if (minuto >= 1 && minuto <= 30)
					this.s.aggiornaVisible();
				else if (minuto > 30){
					this.s.aggiornaVisible();
					this.s.aggiornaVisible();
				}
				break;
			case 14:
				this.s.setVisible(8);
				if (minuto >= 1 && minuto <= 30)
					this.s.aggiornaVisible();
				break;
			}
		}
	}
}
