Federico De Iaco - N. Mat. 845587
Valeria Froio - N. Mat. 847272

Progetto di sistemi distribuiti - Traccia scelta: Traccia A

Eseguire la classe Main.java per poter eseguire l'intero progetto.

I file denotati con "VB" contengono il codice utilizzato per sviluppare la Versione Base del progetto, i file denotati 
con "U1" per sviluppare l'Upgrade 1 e i file denotati con "U2" per sviluppare l'Upgrade 2.

Classi create per la realizzazione della Versione Base:
	- ClienteVB.java
	- FurgoncinoVB.java
	- MainVB.java
	- PersonaleVB.java
	- SupermercatoVB.java

Classi create per la realizzazione dell' Upgrade 1:
	- ClienteU1.java
	- FurgoncinoU1.java
	- PersonaleU1.java
	- ServerThreadU1.java
	- ServerU1.java
	- SupermercatoU1.java

Classi create per la realizzazione dell' Upgrade 2:
	- ClienteU2.java
	- FurgoncinoU2.java
	- OrarioThreadU2.java
	- PersonaleU2.java
	- ServerThreadU2.java
	- ServerU2.java
	- SupermercatoU2.java

La classe Pacchetto.java viene utilizzata da tutte le classi allo stesso modo, quindi non è stato necessario crearne 
una per ogni parte del progetto.

Implementazione Versione Base:
	Per questa implementazione abbiamo deciso di avere 12 Clienti che sono dei threads, 3 persone del personale 
	che sono dei threads e 2 furgoncini che sono anch'essi dei threads dove ogni furgoncino può trasportare al
	massimo 2 pacchetti nel bagagliaio.
	Abbiamo deciso di implementare un magazzino spedizioni e un magazzino ritiro che contengono rispettivamente
	i pacchetti pronti per essere spediti da un furgoncino e i pacchetti pronti per essere ritirati dal cliente.
	I clienti effettuano un ordine che pseudorandomicamente può essere a domicilio o con ritiro della spesa.
	Il personale impacchetta l'ordine per un tempo pseudorandomico, crea il pacchetto relativo e lo immagazzina
	in uno dei due magazzini del supermercato.
	Il furgoncino carica quanti più pacchetti può caricare nel suo bagagliaio (massimo 2) e li rimuove dal 
	magazzino. Inoltre, consegna la spesa e ci mette un tempo pseudorandomico.
	Se, invece, il cliente ha scelto pseudorandomicamente il ritiro della spesa, quando il suo pacchetto si 
	trova nel magazzino di supermercato lo va a ritirare.

Implementazione Upgrade 1:
	Per questa implementazione è stato aggiunto un server che si occupa di gestire connessioni multiple da parte
	di client (clienti) utilizzando la libreria java.nio.channels.
	Il server fa da intermediario tra il cliente e il supermercato; quando si collega un cliente gli vengono
	richiesti i dati necessari alla prenotazione di un nuovo ordine (Es: Nome utente, domiciliato o ritiro, ...).
	Il server si occupa dunque di comunicare gli ordini al supermercato che conseguentemente impegnerà del 
	personale per impacchettare gli ordini (se disponibili, altrimenti comunica un errore al cliente che termina
	la sua esecuzione), se la prenotazione viene presa in carico, viene lanciato un thread (serverThread) che si 
	occupa di gestire la comunicazione con il cliente (Es: pacchetto spedito, pacchetto consegnato o pacchetto
	pronto per il ritiro).
	Viene poi chiesto al cliente se vuole o meno ritirare il pacchetto relativo al suo ordine.
	Se il cliente decide di non voler effettuare un nuovo ordine viene comunicato al server che ha deciso di
	chiudere la connessione.

Implementazione Upgrade 2:
	Per questa implementazione oltre al server viene aggiunto anche un thread (OrarioThreadU2.java) che si occupa di 
	gestire lo scorrere del tempo e l'aggiornamento dell'attributo "visible" della classe SupermercatoU2.java, che
	permette di visualizzare una parte della tabella con gli slot prenotabili e le prenotazioni relative a
	seconda dell'orario.
	Visualizzazione d'esempio della tabella:

	||   Slot0   |   Slot1   |   Slot2   |   Slot3   |   Slot4   |   Slot5   |   Slot6   |   Slot7   |   Slot8   |   Slot9   ||
	---------------------------------------------------------------------------------------------------------------------------
	||10:01~10:30|10:31~11:00|11:01~11:30|11:31~12:00|12:01~12:30|12:31~13:00|13:01~13:30|13:31~14:00|14:01~14:30|14:31~15:00||
	||     x     |     x     |     x     |           |     x     |           |           |     x     |           |           ||
	||     x     |           |     x     |           |           |           |           |     x     |           |           ||
	||           |           |     x     |           |           |           |           |           |           |           ||
	---------------------------------------------------------------------------------------------------------------------------
	(NOTA: la tabella è così visualizzabile solo se il cliente si collega al server tra le 00:00 e le 10:00, in quanto dalle
	10:01 si possono effettuare ordini solo dallo Slot1 in poi)
	

	Come si può evincere dall'esempio sopra illustrato abbiamo deciso di creare 10 slot (da 0 a 9) ognuno dei 
	quali dura mezz'ora. Gli slot vanno dalle 10:01 alle 15:00.
	Ogni slot può contenere al massimo una coda di tre ordini ('x' --> indica che è presente un ordine).
	Se il cliente si collega dalle 10:01 in poi vengono mostrati gli slot utili per effettuare un ordine
	(Es: se il cliente si collega alle 12:43 vengono mostrati gli slot dal numero 6 in poi).
	Se il cliente si collega dopo le 14:31 viene stampato un messaggio di errore e viene chiusa la connessione
	con il server.
	Il server può essere eseguito in qualunque momento della giornata, in quanto viene settato l'attributo
	"visible" in base all'orario in cui viene avviato.
	E' preferibile non modificare l'ora di sistema durante l'esecuzione del server, in quanto potrebbero esserci
	problemi nell'aggiornamento dell'attributo "visible" e quindi nella visualizzazione della tabella.
	Ad ogni aggiornamento di "visible" vengono soddisfatti gli ordini contenuti in "visible" - 1, se quindi 
	"visible" cambia da 0 a 1, perchè sono le 10:01, i clienti che vogliono fare un ordine vedranno la tabella 
	dallo slot 1 in poi, i furgoncini si occuperanno di spedire gli ordini a domicilio dello slot 0, mentre
	ai clienti che hanno scelto il ritiro presso il supermercato nello slot 0 verrà loro notificata la possibilità 
	di ritirare il pacchetto (NOTA: il cliente deve ritirare il pacchetto entro l'orario limite dello slot
	scelto).
	Abbiamo deciso di associare due persone di Personale ad ogni slot, essi verranno richiamati ad impacchettare
	solo gli ordini relativi al loro slot.
	
	
	